# RELATION MICRO SERVICE CONSOLE

## Console

```sh
$ bin/app rm

Psr-4 (no trailing slash !!!): ECommerce
Vendor service folder path (relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-address
Service name (CamelCase !!!): Address
Service prefix (lower case !!!): adr
Vendor sub services folders paths (coma separated, relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-list-country,vendor/paneric/e-commerce-list-type-company
Sub services names (CamelCase, coma separated !!!): ListCountry,ListTypeCompany
Sub services prefixes (lower case, coma separated !!!): lc,ltc
DAO attributes names (camelCase, coma separated !!!): ref,name,surname
DAO attributes types (CamelCase, coma separated !!!): String,String,String
DAO unique attribute name (camelCase !!!): ref

                                                                          
  RELATION MICRO SERVICE UPDATE SUCCESS:                                  
                                                                          

  Resources update with vendor "ECommerce", service "Address" and prefix "adr" success. 
```

## Proces logic

### (1)
Statements blocs keys names contains names of required input variables:

*src/config/settings/statements/dao_statements.php*
```php

<?php

return [

    //=========================================
    'SubService' => [ //block of statements key name as only $SubService is used
        //=========================================

        //----------------------------------------
        'daoUseRelations' => [ // statement to be replaced using only $SubService 
            //----------------------------------------
            ...
        ],
        ...

    ],

    //=========================================
    'attribute_Type' => [  //block of statements key name as $attribute and $Type are used
        //=========================================

        //----------------------------------------
        'daoAttributes' => [ // statement name to be replaced using $attribute and $Type
            //----------------------------------------
            ...
        ],
        ...
    ],
];
```

### (2)

*src/Service/Statement/Preparator/DAOPreparator.php*
```php
<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class DAOPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices, array $attributes, array $Types): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['SubService']; //Block of statements using $SubService variable

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_SubService($statement, $SubServices);
        }

        $subStatements = $statements['attribute_Type']; //Block of statements using $attribute and $Type variables

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_attribute_Type($statement, $attributes, $Types);
        }

        return $stringifiedStatements;
    }
}
```