<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Command;

use Paneric\RMSConsole\Service\RMSService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Container\ContainerInterface;

class RMSCreateCommand extends Command
{
    protected static $defaultName = 'rms';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Creates autonomous relation module.')
            ->setHelp('This command allows autonomous relation module.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Choice of required class.',
                'rms'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        $questionHelper = $this->getHelper('question');
//        $vendor = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Psr-4 (no trailing slash !!!): ')
//        );
//
//        $vendorRelationServicePath = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Vendor service folder path (relative to project folder, no trailing slash !!!): ')
//        );
//        $service = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Service name (CamelCase !!!): ')
//        );
//        $prefix = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Service prefix (lower case !!!): ')
//        );
//
//        $vendorSubServicesPaths = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Vendor relations folders paths (coma separated, relative to project folder, no trailing slash !!!): ')
//        );
//        $subServices = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Relations names (CamelCase, coma separated !!!): ')
//        );
//        $subPrefixes = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Relations prefixes (lower case, coma separated !!!): ')
//        );
//
//        $attributes = $questionHelper->ask(
//            $input,
//            $output,
//            new Question(
//                'DAO attributes names or leave empty (camelCase, coma separated !!!): ',
//                'none'
//            )
//        );
//        $attributesTypes = $questionHelper->ask(
//            $input,
//            $output,
//            new Question(
//                'DAO attributes types or leave empty (CamelCase, coma separated !!!): ',
//                'none'
//            )
//        );
//        $attributesUniques = $questionHelper->ask(
//            $input,
//            $output,
//            new Question(
//                'DAO unique attribute name(s) or leave empty (camelCase, coma separated !!!): ',
//                'none'
//            )
//        );

        $adapter = $this->container->get(RMSService::class);

        if ($adapter === null) {
            $this->setModeErrorOutput($output);

            return 0;
        }

        $vendor = 'ECommerce';
        $vendorRelationServicePath = 'vendor/paneric/e-commerce-address';
        $service = 'Address';
        $prefix = 'adr';
        $vendorSubServicesPaths = 'vendor/paneric/e-commerce-list-country,vendor/paneric/e-commerce-list-type-company';
        $subServices = 'ListCountry,ListTypeCompany';
        $subPrefixes = 'lc,ltc';
        $attributes = 'ref,name,surname';
        $attributesTypes = 'String,String,String';
        $attributesUniques = 'ref,name,surname';

        $adapter->convert(
            $output,
            $vendor,
            $vendorRelationServicePath,
            $service,
            $prefix,
            explode(',', str_replace(' ','', $vendorSubServicesPaths)),
            explode(',', str_replace(' ','', $subServices)),
            explode(',', str_replace(' ','', $subPrefixes)),
            $attributes === 'none' ?
                [] :
                explode(',', str_replace(' ','', $attributes)),
            $attributesTypes === 'none' ?
                [] :
                explode(',', str_replace(' ','', $attributesTypes)),
            $attributesUniques === 'none' ?
                [] :
                explode(',', str_replace(' ','', $attributesUniques)),
        );

        return 0;
    }

    protected function setModeErrorOutput(OutputInterface $output): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('white', 'red', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setModeErrorMessage(),
            ''
        ]);
    }

    protected function setModeErrorMessage(): string
    {
        return '<fg=red;options=bold> Something\'s wrong, check logs. </>';
    }
}

