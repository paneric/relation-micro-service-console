<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApp\config;

use Paneric\Interfaces\Config\ConfigInterface;

class AddressAppControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => 'adr'
        ];
    }
}
