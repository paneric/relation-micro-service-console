<?php

declare(strict_types=1);

use ECommerce\Address\Gateway\AddressADTO;

return [

    'validation' => [

        'adr.add' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'adrs.add' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'adr.edit' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'adrs.edit' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'adr.remove' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        'adrs.remove' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
