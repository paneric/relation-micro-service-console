<?php

declare(strict_types=1);

use Paneric\ComponentModule\Module\Action\SubData;
use Paneric\ComponentModule\Module\Action\App\GetAllAppAction;
use Paneric\DBAL\Manager;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;

use ECommerce\ListCountry\config\ListCountryRepositoryConfig;
use ECommerce\ListCountry\Repository\ListCountryRepository;
use ECommerce\ListCountry\ListCountryApp\config\ListCountryAppActionConfig; 
use ECommerce\ListTypeCompany\config\ListTypeCompanyRepositoryConfig;
use ECommerce\ListTypeCompany\Repository\ListTypeCompanyRepository;
use ECommerce\ListTypeCompany\ListTypeCompanyApp\config\ListTypeCompanyAppActionConfig; 

return [
    'address_app_sub_data' => static function (ContainerInterface $container): SubData
    {
        $listCountryAppSubData = new GetAllAppAction(
            new ListCountryRepository(
                $container->get(Manager::class),
                new ListCountryRepositoryConfig()
            ),
            $container->get(SessionInterface::class),
            new ListCountryAppActionConfig()
        ); 
        $listTypeCompanyAppSubData = new GetAllAppAction(
            new ListTypeCompanyRepository(
                $container->get(Manager::class),
                new ListTypeCompanyRepositoryConfig()
            ),
            $container->get(SessionInterface::class),
            new ListTypeCompanyAppActionConfig()
        ); 

        return new SubData([
            $listCountryAppSubData, 
            $listTypeCompanyAppSubData, 

        ]);
    },
];
