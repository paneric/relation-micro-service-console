<?php

declare(strict_types=1);

namespace ECommerce\Address\config;

use ECommerce\Address\Gateway\AddressDAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class RelationServiceRepositoryConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'table' => 'addresss',
            'dao_class' => AddressDAO::class,
            'fetch_mode' => PDO::FETCH_CLASS,//Data populated before dao constructor
            'create_unique_where' => sprintf(
                ' %s',
                'WHERE adr_ref=:adr_ref AND adr_name=:adr_name AND adr_surname=:adr_surname'
            ),
            'update_unique_where' => sprintf(
                ' %s %s',
                'WHERE adr_ref=:adr_ref AND adr_name=:adr_name AND adr_surname=:adr_surname',
                'AND adr_id NOT IN (:adr_id)'
            ),
            'select_query' => '
                SELECT *
                FROM addresss bt
                    INNER JOIN list_country stlc on bt.adr_list_country_id = stlc.lc_id 
                    INNER JOIN list_type_company stltc on bt.adr_list_type_company_id = stltc.ltc_id 

            ',
        ];
    }
}
