<?php

declare(strict_types=1);

namespace ECommerce\Address\Gateway;

use ECommerce\ListCountry\Gateway\ListCountryDAO; 
use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDAO; 

use Paneric\DataObject\ADAO;

class AddressDAO extends ADAO
{
    protected $id;
    protected $listCountryId; 
    protected $listTypeCompanyId; 

    protected $listCountry; 
    protected $listTypeCompany; 

    protected $ref;//String 
    protected $name;//String 
    protected $surname;//String 

    public function __construct()
    {
        $this->prefix = 'adr_';

        $this->setMaps();

        if ($this->values) {
            $this->listCountry = new ListCountryDAO();
            $this->values = $this->listCountry->hydrate($this->values); 
            $this->listTypeCompany = new ListTypeCompanyDAO();
            $this->values = $this->listTypeCompany->hydrate($this->values); 

            $this->hydrate($this->values);

            unset($this->values);
        }
    }

    /**
     * @return null|int|string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return null|int|string
     */
    public function getListCountryId()
    {
        return $this->listCountryId;
    } 
    /**
     * @return null|int|string
     */
    public function getListTypeCompanyId()
    {
        return $this->listTypeCompanyId;
    } 

    public function getListCountry(): ?ListCountry
    {
        return $this->listCountry;
    } 
    public function getListTypeCompany(): ?ListTypeCompany
    {
        return $this->listTypeCompany;
    } 

    public function getRef(): ?String
    {
        return $this->ref;
    } 
    public function getName(): ?String
    {
        return $this->name;
    } 
    public function getSurname(): ?String
    {
        return $this->surname;
    } 

    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @var int|string
     */
    public function setListCountryId($listCountryId): void
    {
        $this->listCountryId = $listCountryId;
    } 
    /**
     * @var int|string
     */
    public function setListTypeCompanyId($listTypeCompanyId): void
    {
        $this->listTypeCompanyId = $listTypeCompanyId;
    } 

    public function setListCountry(ListCountry $listCountry): void
    {
        $this->listCountry = $listCountry;
    } 
    public function setListTypeCompany(ListTypeCompany $listTypeCompany): void
    {
        $this->listTypeCompany = $listTypeCompany;
    } 

    public function setRef(String $ref): void
    {
        $this->ref = $ref;
    } 
    public function setName(String $name): void
    {
        $this->name = $name;
    } 
    public function setSurname(String $surname): void
    {
        $this->surname = $surname;
    } 

}
