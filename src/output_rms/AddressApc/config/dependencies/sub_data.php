<?php

declare(strict_types=1);

use Paneric\ComponentModule\Module\Action\Apc\GetAllApcAction;
use Paneric\ComponentModule\Module\Action\SubData;

use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;

use ECommerce\ListCountry\ListCountryApc\config\ListCountryApcActionConfig; 
use ECommerce\ListTypeCompany\ListTypeCompanyApc\config\ListTypeCompanyApcActionConfig; 

return [
    'address_apc_sub_data' => static function (ContainerInterface $container): SubData
    {
        $listCountryApcSubData = new GetAllApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            new ListCountryApcActionConfig()
        ); 
        $listTypeCompanyApcSubData = new GetAllApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            new ListTypeCompanyApcActionConfig()
        ); 

        return new SubData([
            $listCountryApcSubData, 
            $listTypeCompanyApcSubData, 

        ]);
    },
];
