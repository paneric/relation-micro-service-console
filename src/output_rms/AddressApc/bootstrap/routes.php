<?php

declare(strict_types=1);

use ECommerce\Address\AddressApc\Controller\AddressApcController;
use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;

if (isset($app, $container)) {

    $app->map(['GET'], '/apc-adr/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(AddressApcController::class)->showOneById(
            $request,
            $response,
            $this->get('address_get_one_by_id_apc_action'),
            $args['id'] ?? '1'
        );
    })->setName('apc-adr.show-one-by-id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET'],'/apc-adrs/show-all', function(Request $request, Response $response) {
        return $this->get(AddressApcController::class)->showAll(
            $request,
            $response,
            $this->get('address_get_all_apc_action')
        );
    })->setName('apc-adrs.show-all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET'],'/apc-adrs/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(AddressApcController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('address_get_all_paginated_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-adrs.show-all-paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class)); // no pagination middleware !!!


    $app->map(['GET', 'POST'], '/apc-adr/add', function(Request $request, Response $response) {
        return $this->get(AddressApcController::class)->add(
            $request,
            $response,
            $this->get('address_apc_sub_data'),
            $this->get('address_create_apc_action')
        );
    })->setName('apc-adr.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-adrs/add', function(Request $request, Response $response) {
        return $this->get(AddressApcController::class)->addMultiple(
            $request,
            $response,
            $this->get('address_apc_sub_data'),
            $this->get('address_create_multiple_apc_action')
        );
    })->setName('apc-adrs.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-adr/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(AddressApcController::class)->edit(
            $request,
            $response,
            $this->get('address_apc_sub_data'),
            $this->get('address_get_one_by_id_apc_action'),
            $this->get('address_update_apc_action'),
            $args['id']
        );
    })->setName('apc-adr.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-adrs/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(AddressApcController::class)->editMultiple(
            $request,
            $response,
            $this->get('address_apc_sub_data'),
            $this->get('address_get_all_paginated_apc_action'),
            $this->get('address_update_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-adrs.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-adr/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(AddressApcController::class)->remove(
            $request,
            $response,
            $this->get('address_delete_apc_action'),
            $args['id']
        );
    })->setName('apc-adr.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-adrs/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(AddressApcController::class)->removeMultiple(
            $request,
            $response,
            $this->get('address_get_all_paginated_apc_action'),
            $this->get('address_delete_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-adrs.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}

