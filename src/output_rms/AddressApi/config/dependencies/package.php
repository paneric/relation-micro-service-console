<?php

declare(strict_types=1);

use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;
use Paneric\Guard\Guard;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Session\DBAL\DBALAdapter;
use Paneric\Session\DBAL\SessionRepository;
use Paneric\Session\SessionWrapper;
use Psr\Container\ContainerInterface;

return [
    Manager::class => static function (ContainerInterface $container): Manager
    {
        $pdoBuilder = new PDOBuilder();

        return new Manager(
            $pdoBuilder->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },

    GuardInterface::class => static function (ContainerInterface $container): Guard
    {
        $randomFactory = new RandomLib\Factory;

        return new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            $container->get('guard')
        );
    },

    SessionInterface::class => static function (ContainerInterface $container): ?SessionWrapper
    {
        $pdoAdapter = new DBALAdapter(
            $container->get(Manager::class),
            new SessionRepository($container->get(Manager::class)),
            $container->get(GuardInterface::class)
        );

        return new SessionWrapper(
            $container->get('session-wrapper'),
            $pdoAdapter
        );
    },
];
