<?php

declare(strict_types=1);

use ECommerce\Address\Gateway\AddressADTO;

return [

    'validation' => [

        'api-adr.create' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-adrs.create' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-adr.update' => [
            'methods' => ['PUT'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-adrs.update' => [
            'methods' => ['PUT'],
            AddressADTO::class => [
                'rules' => [
                    'list_country_id' => [
                        'required' => [],                    
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],
                    ], 
                    'name' => [
                        'required' => [],
                    ], 
                    'surname' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-adrs.delete' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ]
                ],
            ],
        ],
    ]
];
