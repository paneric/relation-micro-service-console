<?php

declare(strict_types=1);

use ECommerce\Address\AddressApi\Controller\AddressApiController;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->map(['GET'], '/api-adr/get/{id}', function(Request $request, Response $response, array $args){
        return $this->get(AddressApiController::class)->getOneById(
            $request,
            $response,
            $this->get('address_get_one_by_id_api_action'),
            $args['id']
        );
    })->setName('api-adr.get')
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->map(['GET'], '/api-adrs/get', function(Request $request, Response $response){
        return $this->get(AddressApiController::class)->getAll(
            $request,
            $response,
            $this->get('address_get_all_api_action')
        );
    })->setName('api-adrs.get')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->map(['GET'], '/api-adrs/get/{page}', function(Request $request, Response $response, array $args){
        return $this->get(AddressApiController::class)->getAllPaginated(
            $request,
            $response,
            $this->get('address_get_all_paginated_api_action'),
            $args['page']
        );
    })->setName('api-adrs.get.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->post('/api-adr/create', function(Request $request, Response $response){
        return $this->get(AddressApiController::class)->create(
            $request,
            $response,
            $this->get('address_create_api_action')
        );
    })->setName('api-adr.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-adrs/create', function(Request $request, Response $response){
        return $this->get(AddressApiController::class)->createMultiple(
            $request,
            $response,
            $this->get('address_create_multiple_api_action')
        );
    })->setName('api-adrs.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->put('/api-adr/update/{id}', function(Request $request, Response $response, array $args){
        return $this->get(AddressApiController::class)->update(
            $request,
            $response,
            $this->get('address_update_api_action'),
            $args['id']
        );
    })->setName('api-adr.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-adrs/update', function(Request $request, Response $response){
        return $this->get(AddressApiController::class)->updateMultiple(
            $request,
            $response,
            $this->get('address_update_multiple_api_action')
        );
    })->setName('api-adrs.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->delete('/api-adr/delete/{id}', function(Request $request, Response $response, array $args){
        return $this->get(AddressApiController::class)->delete(
            $request,
            $response,
            $this->get('address_delete_api_action'),
            $args['id']
        );
    })->setName('api-adr.delete')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-adrs/delete', function(Request $request, Response $response){
        return $this->get(AddressApiController::class)->deleteMultiple(
            $request,
            $response,
            $this->get('address_delete_multiple_api_action')
        );
    })->setName('api-adrs.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));
}
