<?php

return [
    'rms' => [
        'files' => [
            APP_FOLDER . 'resources_rms/Gateway/RelationServiceDAO.php' => 'dao',

            APP_FOLDER . 'resources_rms/RelationServiceApi/config/settings/validation.php' => 'settings_validation',
            APP_FOLDER . 'resources_rms/RelationServiceApp/config/settings/validation.php' => 'settings_validation',

            APP_FOLDER . 'resources_rms/RelationServiceApc/config/dependencies/sub_data.php' => 'sub_data_apc',
            APP_FOLDER . 'resources_rms/RelationServiceApp/config/dependencies/sub_data.php' => 'sub_data_app',

            APP_FOLDER . 'resources_rms/config/module-resolver-config.php' => 'module_resolver_config',

            APP_FOLDER . 'resources_rms/RelationServiceApi/config/RelationServiceApiActionConfig.php' => 'action_config',
            APP_FOLDER . 'resources_rms/RelationServiceApp/config/RelationServiceAppActionConfig.php' => 'action_config',

            APP_FOLDER . 'resources_rms/config/RelationServiceRepositoryConfig.php' => 'repository_config',

//            APP_FOLDER . 'resources_rms/RelationServiceApc/templates/add.html.twig'                => 'templates_add',
//            APP_FOLDER . 'resources_rms/RelationServiceApp/templates/add.html.twig'                => 'templates_add',
//            APP_FOLDER . 'resources_rms/RelationServiceApc/templates/edit.html.twig'               => 'templates_edit',
//            APP_FOLDER . 'resources_rms/RelationServiceApp/templates/edit.html.twig'               => 'templates_edit',
//            APP_FOLDER . 'resources_rms/RelationServiceApc/templates/show_all_paginated.html.twig' => 'templates_get_all_paginated',
//            APP_FOLDER . 'resources_rms/RelationServiceApp/templates/show_all_paginated.html.twig' => 'templates_get_all_paginated',
//            APP_FOLDER . 'resources_rms/RelationServiceApc/templates/show_all.html.twig'           => 'templates_get_all',
//            APP_FOLDER . 'resources_rms/RelationServiceApp/templates/show_all.html.twig'           => 'templates_get_all',
        ],

        'params' => [
            'patterns' => ['/{Paneric}/', '/{vendorRelationServicePath}/', '/{relation_service}/', '/{RelationService}/', '/{prefix}/',],
            'methods'  => [        'set',                           'set',          'set_lc_sc',                 'set',          'set',],
            'values'   => [     'vendor',     'vendorRelationServicePath',            'service',             'service',       'prefix',],
        ],

        'dao' => require 'statements/dao_statements.php',

        'settings_validation' => require 'statements/validation_statements.php',

        'sub_data_apc' => require 'statements/sub_data_apc_statements.php',
        'sub_data_app' => require 'statements/sub_data_app_statements.php',

        'module_resolver_config' => require 'statements/module_resolver_config_statements.php',

        'action_config' =>  require 'statements/action_config_statements.php',

        'repository_config' =>  require 'statements/repository_config_statements.php',

//        'templates_add'               => require 'statements/templates_add_statements.php',
//        'templates_edit'              => require 'statements/templates_edit_statements.php',
//        'templates_get_all_paginated' => require 'statements/templates_get_all_paginated_statements.php',
//        'templates_get_all'           => require 'statements/templates_get_all_statements.php',
    ],
];
