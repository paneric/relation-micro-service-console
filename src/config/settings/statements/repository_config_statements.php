<?php

return [

    //=========================================
    'attributeUnique' => [
        //=========================================

        //----------------------------------------
        'uniqueCriteriaSQLWhere' => [
            //----------------------------------------
            'template' =>
<<<END
{prefix}_{attribute_unique}=:{prefix}_{attribute_unique} AND 
END,
            'patterns' => ['/{attribute_unique}/'],
            'methods'  => [           'set_lc_sc'],
            'values'   => [     'attributeUnique'],
        ],
    ],

    //=========================================
    'SubService_subprefix' => [
        //=========================================

        //----------------------------------------
        'innerJoinSubService' => [
            //----------------------------------------
            'template' =>
<<<END
                    INNER JOIN {sub_service} st{sub_prefix} on bt.{prefix}_{sub_service}_id = st{sub_prefix}.{sub_prefix}_id \n
END,
            'patterns' => ['/{sub_service}/', '/{sub_prefix}/'],
            'methods'  => [      'set_lc_sc',            'set'],
            'values'   => [     'SubService',      'subprefix'],
        ],
    ],

];
