<?php

return [
    //=========================================
    'SubService' => [
        //=========================================

        //----------------------------------------
        'validationRelationsId' => [
            //----------------------------------------
            'template' =>
<<<END
                    '{sub_service}_id' => [
                        'required' => [],                    
                    ], \n
END,
            'patterns' => ['/{sub_service}/'],
            'methods'  => [      'set_lc_sc'],
            'values'   => [     'SubService'],
        ],
    ],

    //=========================================
    'attribute' => [
        //=========================================

        //----------------------------------------
        'validationAttributes' => [
            //----------------------------------------
            'template' =>
<<<END
                    '{attribute}' => [
                        'required' => [],
                    ], \n
END,
            'patterns' => ['/{attribute}/'],
            'methods'  => [    'set_lc_sc'],
            'values'   => [    'attribute'],
        ],
    ],
];
