<?php

return [

    //=========================================
    'SubService' => [
        //=========================================

        //----------------------------------------
        'subDataUseSubServices' => [
            //----------------------------------------
            'template' =>
<<<END
use {Paneric}\{SubService}\config\{SubService}RepositoryConfig;
use {Paneric}\{SubService}\Repository\{SubService}Repository;
use {Paneric}\{SubService}\{SubService}App\config\{SubService}AppActionConfig; \n
END,
            'patterns' => ['/{SubService}/'],
            'methods'  => [           'set'],
            'values'   => [    'SubService'],
        ],

        //----------------------------------------
        'subDataSubServiceCreate' => [
            //----------------------------------------
            'template' =>
<<<END
        \${subService}AppSubData = new GetAllAppAction(
            new {SubService}Repository(
                \$container->get(Manager::class),
                new {SubService}RepositoryConfig()
            ),
            \$container->get(SessionInterface::class),
            new {SubService}AppActionConfig()
        ); \n
END,
            'patterns' => ['/{subService}/', '/{SubService}/'],
            'methods'  => [       'set_lcf',            'set'],
            'values'   => [    'SubService',     'SubService'],
        ],

        //----------------------------------------
        'subDataSubService' => [
            //----------------------------------------
            'template' =>
<<<END
            \${subService}AppSubData, \n
END,
            'patterns' => ['/{subService}/'],
            'methods'  => [       'set_lcf'],
            'values'   => [    'SubService'],
        ],

    ]
];
