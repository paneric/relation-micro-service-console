<?php

return [

    'vendorSubServicePath' => [

        'vendorSubServicesPaths' => [

            'template' =>
<<<END
    '{vendorSubServicePath}', \n
END,
            'patterns' => ['/{vendorSubServicePath}/'],
            'methods'  => [                     'set'],
            'values'   => [    'vendorSubServicePath'],
        ],
    ],
];
