<?php

return [

    //=========================================
    'attributeUnique' => [
        //=========================================

        //----------------------------------------
        'uniqueCriteriaFromAttributes' => [
            //----------------------------------------
            'template' =>
<<<END
                        '{prefix}_{attribute_unique}' => \$attributes['{attribute_unique}'], \n
END,
            'patterns' => ['/{attribute_unique}/'],
            'methods'  => [           'set_lc_sc'],
            'values'   => [     'attributeUnique'],
        ],

        //----------------------------------------
        'uniqueCriteriaFromDAO' => [
            //----------------------------------------
            'template' =>
<<<END
                            '{prefix}_{attribute_unique}' => \$dao->get{AttributeUnique}(), \n
END,
            'patterns' => ['/{attribute_unique}/', '/{AttributeUnique}/'],
            'methods'  => [           'set_lc_sc',             'set_UCF'],
            'values'   => [     'attributeUnique',     'attributeUnique'],
        ],


    ],

];
