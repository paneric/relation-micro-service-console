<?php

return [

    //=========================================
    'SubService' => [
        //=========================================

        //----------------------------------------
        'subDataUseSubServices' => [
            //----------------------------------------
            'template' =>
<<<END
use {Paneric}\{SubService}\{SubService}Apc\config\{SubService}ApcActionConfig; \n
END,
            'patterns' => ['/{SubService}/'],
            'methods'  => [           'set'],
            'values'   => [    'SubService'],
        ],

        //----------------------------------------
        'subDataSubServiceCreate' => [
            //----------------------------------------
            'template' =>
<<<END
        \${subService}ApcSubData = new GetAllApcAction(
            \$container->get(HttpClientManager::class),
            \$container->get(SessionInterface::class),
            new {SubService}ApcActionConfig()
        ); \n
END,
            'patterns' => ['/{subService}/', '/{SubService}/'],
            'methods'  => [       'set_lcf',            'set'],
            'values'   => [    'SubService',     'SubService'],
        ],

        //----------------------------------------
        'subDataSubService' => [
            //----------------------------------------
            'template' =>
<<<END
            \${subService}ApcSubData, \n
END,
            'patterns' => ['/{subService}/'],
            'methods'  => [       'set_lcf'],
            'values'   => [    'SubService'],
        ],

    ]
];
