<?php

return [

    //=========================================
    'SubService' => [
        //=========================================

        //----------------------------------------
        'daoUseRelations' => [
            //----------------------------------------
            'template' =>
<<<END
use {Paneric}\{SubService}\Gateway\{SubService}DAO; \n
END,
            'patterns' => ['/{SubService}/'],
            'methods'  => [           'set'],
            'values'   => [    'SubService'],
        ],

        //----------------------------------------
        'daoRelationsId' => [
            //----------------------------------------
            'template' =>
<<<END
    protected \${subService}Id; \n
END,
            'patterns' => ['/{subService}/'],
            'methods'  => [       'set_lcf'],
            'values'   => [    'SubService'],
        ],

        //----------------------------------------
        'daoRelations' => [
            //----------------------------------------
            'template' =>
<<<END
    protected \${subService}; \n
END,
            'patterns' => ['/{subService}/'],
            'methods'  => [       'set_lcf'],
            'values'   => [    'SubService'],
        ],

        //----------------------------------------
        'daoRelationsSeed' => [
            //----------------------------------------
            'template' =>
<<<END
            \$this->{subService} = new {SubService}DAO();
            \$this->values = \$this->{subService}->hydrate(\$this->values); \n
END,
            'patterns' => ['/{SubService}/', '/{subService}/'],
            'methods'  => [           'set',        'set_lcf'],
            'values'   => [    'SubService',     'SubService'],
        ],

        //----------------------------------------
        'daoGettersRelationsId' => [
            //----------------------------------------
            'template' =>
<<<END
    /**
     * @return null|int|string
     */
    public function get{SubService}Id()
    {
        return \$this->{subService}Id;
    } \n
END,
            'patterns' => ['/{SubService}/', '/{subService}/'],
            'methods'  => [           'set',        'set_lcf'],
            'values'   => [    'SubService',     'SubService'],
        ],

        //----------------------------------------
        'daoGettersRelations' => [
            //----------------------------------------
            'template' =>
<<<END
    public function get{SubService}(): ?{SubService}
    {
        return \$this->{subService};
    } \n
END,
            'patterns' => ['/{SubService}/', '/{subService}/'],
            'methods'  => [           'set',        'set_lcf'],
            'values'   => [    'SubService',     'SubService'],
        ],

        //----------------------------------------
        'daoSettersRelationsId' => [
            //----------------------------------------
            'template' =>
<<<END
    /**
     * @var int|string
     */
    public function set{SubService}Id(\${subService}Id): void
    {
        \$this->{subService}Id = \${subService}Id;
    } \n
END,
            'patterns' => ['/{SubService}/', '/{subService}/'],
            'methods'  => [           'set',        'set_lcf'],
            'values'   => [    'SubService',     'SubService'],
        ],

        //----------------------------------------
        'daoSettersRelations' => [
            //----------------------------------------
            'template' =>
<<<END
    public function set{SubService}({SubService} \${subService}): void
    {
        \$this->{subService} = \${subService};
    } \n
END,
            'patterns' => ['/{SubService}/', '/{subService}/'],
            'methods'  => [           'set',        'set_lcf'],
            'values'   => [    'SubService',     'SubService'],
        ],

    ],

    //=========================================
    'attribute_Type' => [
        //=========================================

        //----------------------------------------
        'daoAttributes' => [
            //----------------------------------------
            'template' =>
<<<END
    protected \${attribute};//{Type} \n
END,
            'patterns' => [ '/{attribute}/',  '/{Type}/'],
            'methods'  => [           'set',       'set'],
            'values'   => [     'attribute',      'Type'],
        ],

        //----------------------------------------
        'daoGettersAttributes' => [
            //----------------------------------------
            'template' =>
<<<END
    public function get{Attribute}(): ?{Type}
    {
        return \$this->{attribute};
    } \n
END,
            'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
            'methods'  => [           'set',        'set_UCF',       'set'],
            'values'   => [     'attribute',      'attribute',      'Type'],
        ],

        //----------------------------------------
        'daoSettersAttributes' => [
            //----------------------------------------
            'template' =>
<<<END
    public function set{Attribute}({Type} \${attribute}): void
    {
        \$this->{attribute} = \${attribute};
    } \n
END,
            'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
            'methods'  => [           'set',        'set_UCF',   'set_UCF'],
            'values'   => [     'attribute',      'attribute',      'Type'],
        ],

    ],
];
