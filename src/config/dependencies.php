<?php

declare(strict_types=1);

use Paneric\RMSConsole\Service\FilesScanner;
use Paneric\RMSConsole\Service\RMSService;
use Paneric\RMSConsole\Service\PatternsConverter;
use Paneric\RMSConsole\Service\Statement\Preparator\DAOPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\ModuleResolverConfigPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\RepositoryConfigPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\SubDataPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\ActionConfigPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\ValidationPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\TemplatesAddEditPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\TemplatesShowAllPaginatedPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\TemplatesShowAllPreparator;
use Paneric\RMSConsole\Service\Statement\StatementsConverter;
use Paneric\RMSConsole\Service\Statement\StatementsPreparator;
use Psr\Container\ContainerInterface;

return [
    RMSService::class => static function (ContainerInterface $container): RMSService
    {
        return new RMSService(
            $container->get(StatementsPreparator::class),
            $container->get(PatternsConverter::class),
            $container->get(StatementsConverter::class),
            $container->get(FilesScanner::class),
            $container->get('rms'),
            $container->get('rms_resources_dir'),
            $container->get('rms_output_dir')
        );
    },

    FilesScanner::class => static function (): FilesScanner
    {
        return new FilesScanner();
    },

    PatternsConverter::class => static function (ContainerInterface $container): PatternsConverter
    {
        return new PatternsConverter(
            $container->get('rms')['params'],
        );
    },

    StatementsPreparator::class => static function (ContainerInterface $container): StatementsPreparator
    {
        return new StatementsPreparator(
            $container->get(DAOPreparator::class),
            $container->get(ValidationPreparator::class),
            $container->get(SubDataPreparator::class),
            $container->get(ModuleResolverConfigPreparator::class),
            $container->get(ActionConfigPreparator::class),
            $container->get(RepositoryConfigPreparator::class),
            $container->get('rms')
        );
    },

    DAOPreparator::class => static function (): DAOPreparator
    {
        return new DAOPreparator();
    },
    ValidationPreparator::class => static function (): ValidationPreparator
    {
        return new ValidationPreparator();
    },
    SubDataPreparator::class => static function (): SubDataPreparator
    {
        return new SubDataPreparator();
    },
    ModuleResolverConfigPreparator::class => static function (): ModuleResolverConfigPreparator
    {
        return new ModuleResolverConfigPreparator();
    },
    ActionConfigPreparator::class => static function (): ActionConfigPreparator
    {
        return new ActionConfigPreparator();
    },
    RepositoryConfigPreparator::class => static function (): RepositoryConfigPreparator
    {
        return new RepositoryConfigPreparator();
    },


    TemplatesAddEditPreparator::class => static function (): TemplatesAddEditPreparator
    {
        return new TemplatesAddEditPreparator();
    },
    TemplatesShowAllPaginatedPreparator::class => static function (): TemplatesShowAllPaginatedPreparator
    {
        return new TemplatesShowAllPaginatedPreparator();
    },
    TemplatesShowAllPreparator::class => static function (): TemplatesShowAllPreparator
    {
        return new TemplatesShowAllPreparator();
    },


    StatementsConverter::class => static function (): StatementsConverter
    {
        return new StatementsConverter();
    },
];
