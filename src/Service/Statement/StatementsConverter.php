<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement;

class StatementsConverter
{
    public function convert(
        string $stringifiedFile,
        array $statements
    ): string {
        $patterns = array_keys($statements);

        foreach ($patterns as $index => $key) {
            $patterns[$index] = '/{' . $key . '}/';
        }

        return preg_replace(
            $patterns,
            array_values($statements),
            $stringifiedFile
        );
    }
}
