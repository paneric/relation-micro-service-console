<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class ActionConfigPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $attributesUniques): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['attributeUnique'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_attributeUnique($statement, $attributesUniques);
        }

        return $stringifiedStatements;
    }
}
