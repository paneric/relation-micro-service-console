<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

use Paneric\RMSConsole\Service\ParamsTrait;
use Paneric\RMSConsole\Service\SettersTrait;

trait PreparatorsTrait
{
    use ParamsTrait;
    use SettersTrait;

    protected function prepareWithArgument(array $statement, array $values, string $argumentName): string
    {
        $stringifiedStatement = '';

        foreach ($values as $value) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                $argumentName => $value
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWithSubServicesSubPrefixes(
        array $statement,
        array $SubServices,
        array $subprefixes
    ): string {
        $stringifiedStatement = '';

        foreach ($SubServices as $index => $SubService) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'SubService' => $SubService,
                'subprefix' => $subprefixes[$index]
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_attribute(array $statement, array $attributes): string
    {
        $stringifiedStatement = '';

        if (empty($attributes)) {
            return $stringifiedStatement;
        }

        foreach ($attributes as $attribute) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'attribute' => $attribute
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_attribute_Type(array $statement, array $attributes, array $Types): string
    {
        $stringifiedStatement = '';

        foreach ($attributes as $index => $attribute) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'attribute' => $attribute,
                'Type' => $Types[$index]
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_SubService(array $statement, array $SubServices): string
    {
        $stringifiedStatement = '';

        if (empty($SubServices)) {
            return $stringifiedStatement;
        }

        foreach ($SubServices as $SubService) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'SubService' => $SubService
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_vendorSubServicePath(array $statement, array $vendorSubServicesPaths): string
    {
        $stringifiedStatement = '';

        if (empty($vendorSubServicesPaths)) {
            return $stringifiedStatement;
        }

        foreach ($vendorSubServicesPaths as $vendorSubServicePath) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'vendorSubServicePath' => $vendorSubServicePath
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_subprefix(array $statement, array $subprefixes): string
    {
        $stringifiedStatement = '';

        if (empty($subprefixes)) {
            return $stringifiedStatement;
        }

        foreach ($subprefixes as $subprefix) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'subprefix' => $subprefix
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_Attributetype(array $statement, array $Attributetypes): string
    {
        $stringifiedStatement = '';

        if (empty($Attributetypes)) {
            return $stringifiedStatement;
        }

        foreach ($Attributetypes as $Attributetype) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'AttributeType' => $Attributetype
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_attributeUnique(array $statement, array $attributesUniques): string
    {
        $stringifiedStatement = '';

        if (empty($attributesUniques)) {
            return $stringifiedStatement;
        }

        foreach ($attributesUniques as $attributeUnique) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'attributeUnique' => $attributeUnique
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWith_SubService_subprefix(array $statement, array $SubServices = [], array $subprefixes = []): string
    {
        $stringifiedStatement = '';

        if (empty($SubServices) && empty($subprefixes)) {
            return $stringifiedStatement;
        }

        foreach ($SubServices as $index => $SubService) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'SubService' => $SubService,
                'subprefix' => $subprefixes[$index]
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }
}
