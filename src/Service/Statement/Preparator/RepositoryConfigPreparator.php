<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class RepositoryConfigPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $attributesUniques, array $SubServices, array $subprefixes): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['attributeUnique'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatement = $this->prepareWith_attributeUnique($statement, $attributesUniques);
            $stringifiedStatements[$key] = '                \'WHERE ' . rtrim($stringifiedStatement, " AND") . "'";
        }

        $subStatements = $statements['SubService_subprefix'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_SubService_subprefix($statement, $SubServices, $subprefixes);
        }

        return $stringifiedStatements;
    }
}
