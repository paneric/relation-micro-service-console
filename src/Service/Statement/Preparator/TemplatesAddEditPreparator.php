<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class TemplatesAddEditPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices, array $attributes, array $subprefixes): array
    {
        $stringifiedStatements['attributes_id'] = $this->prepareWithSubServicesSubPrefixes(
            $statements['attributes_id'],
            $SubServices,
            $subprefixes
        );

        $stringifiedStatements['attributes'] = $this->prepareWithAttributes(
            $statements['attributes'],
            $attributes
        );

        return $stringifiedStatements;
    }
}
