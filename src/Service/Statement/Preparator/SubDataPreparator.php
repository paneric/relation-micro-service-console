<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class SubDataPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['SubService'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_SubService($statement, $SubServices);
        }

        return $stringifiedStatements;
    }
}
