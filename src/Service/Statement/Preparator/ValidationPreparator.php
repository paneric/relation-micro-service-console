<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class ValidationPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices, array $attributes): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['SubService'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_SubService($statement, $SubServices);
        }

        $subStatements = $statements['attribute'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_attribute($statement, $attributes);
        }

        return $stringifiedStatements;
    }
}
