<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class ModuleResolverConfigPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $vendorSubServicesPaths): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['vendorSubServicePath'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_vendorSubServicePath($statement, $vendorSubServicesPaths);
        }

        return $stringifiedStatements;
    }
}
