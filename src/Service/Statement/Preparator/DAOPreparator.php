<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement\Preparator;

class DAOPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices, array $attributes, array $Types): array
    {
        $stringifiedStatements = [];

        $subStatements = $statements['SubService'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_SubService($statement, $SubServices);
        }

        $subStatements = $statements['attribute_Type'];

        foreach ($subStatements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWith_attribute_Type($statement, $attributes, $Types);
        }

        return $stringifiedStatements;
    }
}
