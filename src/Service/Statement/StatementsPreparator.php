<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service\Statement;

use Paneric\RMSConsole\Service\Statement\Preparator\DAOPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\ValidationPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\SubDataPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\ModuleResolverConfigPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\ActionConfigPreparator;
use Paneric\RMSConsole\Service\Statement\Preparator\RepositoryConfigPreparator;

//use Paneric\RMSConsole\Service\Statement\Preparator\TemplatesAddEditPreparator;
//use Paneric\RMSConsole\Service\Statement\Preparator\TemplatesShowAllPaginatedPreparator;
//use Paneric\RMSConsole\Service\Statement\Preparator\TemplatesShowAllPreparator;

class StatementsPreparator
{
    protected $daoPreparator;
    protected $validationPreparator;
    protected $subDataPreparator;
    protected $moduleResolverConfigPreparator;
    protected $actionConfigPreparator;
    protected $repositoryConfigPreparator;
//    protected $templatesAddEditPreparator;
//    protected $templatesShowAllPaginatedPreparator;
//    protected $templatesShowAllPreparator;


    protected $settings;

    public function __construct(
        DAOPreparator                  $daoPreparator,
        ValidationPreparator           $validationPreparator,
        SubDataPreparator              $subDataPreparator,
        ModuleResolverConfigPreparator $moduleResolverConfigPreparator,
        ActionConfigPreparator         $actionConfigPreparator,
        RepositoryConfigPreparator     $repositoryConfigPreparator,
//        TemplatesAddEditPreparator          $templatesAddEditPreparator,
//        TemplatesShowAllPaginatedPreparator $templatesShowAllPaginatedPreparator,
//        TemplatesShowAllPreparator          $templatesShowAllPreparator,

        array                               $settings
    ) {
        $this->daoPreparator                  = $daoPreparator;
        $this->validationPreparator           = $validationPreparator;
        $this->subDataPreparator              = $subDataPreparator;
        $this->moduleResolverConfigPreparator = $moduleResolverConfigPreparator;
        $this->actionConfigPreparator         = $actionConfigPreparator;
        $this->repositoryConfigPreparator     = $repositoryConfigPreparator;
//        $this->templatesAddEditPreparator = $templatesAddEditPreparator;
//        $this->templatesShowAllPaginatedPreparator = $templatesShowAllPaginatedPreparator;
//        $this->templatesShowAllPreparator = $templatesShowAllPreparator;


        $this->settings = $settings;
    }

    public function prepare(
        string $filePath,
        array $vendorSubServicesPaths,
        array $SubServices,
        array $subprefixes,
        array $attributes,
        array $attributesTypes,
        array $attributesUniques
    ): array {
        $statements = [];

        if (isset($this->settings['files'][$filePath])) {
            $converter = $this->settings['files'][$filePath];

            if ($converter === 'dao') {
                $statements = $this->daoPreparator->prepare(
                    $this->settings['dao'],
                    $SubServices,
                    $attributes,
                    $attributesTypes
                );
            }

            if ($converter === 'settings_validation') {
                $statements = $this->validationPreparator->prepare(
                    $this->settings['settings_validation'],
                    $SubServices,
                    $attributes
                );
            }

            if ($converter === 'sub_data_apc') {
                $statements = $this->subDataPreparator->prepare(
                    $this->settings['sub_data_apc'],
                    $SubServices
                );
            }

            if ($converter === 'sub_data_app') {
                $statements = $this->subDataPreparator->prepare(
                    $this->settings['sub_data_app'],
                    $SubServices
                );
            }

            if ($converter === 'module_resolver_config') {
                $statements = $this->moduleResolverConfigPreparator->prepare(
                    $this->settings['module_resolver_config'],
                    $vendorSubServicesPaths
                );
            }

            if ($converter === 'action_config') {
                $statements = $this->actionConfigPreparator->prepare(
                    $this->settings['action_config'],
                    $attributesUniques
                );
            }

            if ($converter === 'repository_config') {
                $statements = $this->repositoryConfigPreparator->prepare(
                    $this->settings['repository_config'],
                    $attributesUniques,
                    $SubServices,
                    $subprefixes
                );
            }






//            if ($converter === 'templates_add') {
//                $statements = $this->templatesAddEditPreparator->prepare(
//                    $this->settings['templates_add'],
//                    $SubServices,
//                    $attributes,
//                    $subprefixes
//                );
//            }
//
//            if ($converter === 'templates_edit') {
//                $statements = $this->templatesAddEditPreparator->prepare(
//                    $this->settings['templates_edit'],
//                    $SubServices,
//                    $attributes,
//                    $subprefixes
//                );
//            }
//
//            if ($converter === 'templates_get_all_paginated') {
//                $statements = $this->templatesShowAllPaginatedPreparator->prepare(
//                    $this->settings['templates_get_all_paginated'],
//                    $SubServices,
//                    $attributes,
//                    $subprefixes
//                );
//            }
//
//            if ($converter === 'templates_get_all') {
//                $statements = $this->templatesShowAllPreparator->prepare(
//                    $this->settings['templates_get_all'],
//                    $SubServices,
//                    $attributes,
//                    $subprefixes
//                );
//            }
//

        }

        return $statements;
    }
}
