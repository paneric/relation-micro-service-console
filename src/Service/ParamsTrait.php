<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service;

use RuntimeException;

trait ParamsTrait
{
    protected function setParams(array $extractor): array
    {
        foreach ($extractor as $key => $variable) {
            ${$key} = $variable;
        }

        $params = [];

        foreach ($methods as $key => $method) {
            $paramName = $values[$key];

            $params[$key] = $this->$method(${$paramName});
        }

        if ($params === []) {
            throw new RuntimeException('Paneric\\Array of values is not defined');
        }

        return $params;
    }
}
