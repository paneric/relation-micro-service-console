<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service;

class PatternsConverter
{
    use ParamsTrait;
    use SettersTrait;

    protected $settings;

    public function __construct(array $settings
    ) {
        $this->settings = $settings;
    }

    public function convert(
        string $stringifiedFile,
        string $vendor,
        string $vendorRelationServicePath,
        string $service,
        string $prefix,
        array $subPrefixes
    ): string {
        $extractor = [
            'methods' => $this->settings['methods'],
            'values' => $this->settings['values'],
            'vendor' => $vendor,
            'vendorRelationServicePath' => $vendorRelationServicePath,
            'service' => $service,
            'prefix' => $prefix,
            'subPrefixes' => $this->setComaSubPrefixes($subPrefixes),
        ];

        return preg_replace(
            $this->settings['patterns'],
            $this->setParams($extractor),
            $stringifiedFile
        );
    }
}
