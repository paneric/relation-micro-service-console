<?php

declare(strict_types=1);

namespace Paneric\RMSConsole\Service;

class FilesScanner
{
    public function scanDirectory(string $path, &$name = []): array
    {
        $lists = scandir($path);

        if(!empty($lists)) {
            foreach($lists as $f) {
                if($f !== "."  && $f !== ".." && is_dir($path . DIRECTORY_SEPARATOR . $f)) {
                    $this->scanDirectory($path . DIRECTORY_SEPARATOR . $f, $name);
                }
                if ($f !== "."  && $f !== ".." && !is_dir($path . DIRECTORY_SEPARATOR . $f)){
                    $name[] = $path.DIRECTORY_SEPARATOR.$f;
                }
            }
        }

        return $name;
    }
}
