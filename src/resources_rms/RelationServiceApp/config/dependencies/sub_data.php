<?php

declare(strict_types=1);

use Paneric\ComponentModule\Module\Action\SubData;
use Paneric\ComponentModule\Module\Action\App\GetAllAppAction;
use Paneric\DBAL\Manager;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;

{subDataUseSubServices}
return [
    '{relation_service}_app_sub_data' => static function (ContainerInterface $container): SubData
    {
{subDataSubServiceCreate}
        return new SubData([
{subDataSubService}
        ]);
    },
];
