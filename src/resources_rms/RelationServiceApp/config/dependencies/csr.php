<?php

use {Paneric}\{RelationService}\{RelationService}App\config\{RelationService}AppActionConfig;
use {Paneric}\{RelationService}\{RelationService}App\config\{RelationService}AppControllerConfig;
use {Paneric}\{RelationService}\{RelationService}App\Controller\{RelationService}AppController;
use {Paneric}\{RelationService}\Repository\{RelationService}RepositoryInterface;
use Paneric\ComponentModule\Module\Action\App\CreateAppAction;
use Paneric\ComponentModule\Module\Action\App\CreateMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllPaginatedAppAction;
use Paneric\ComponentModule\Module\Action\App\GetOneByIdAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateMultipleAppAction;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;

return [

    {RelationService}AppController::class => static function(ContainerInterface $container): {RelationService}AppController
    {
        return new {RelationService}AppController(
            $container->get(Twig::class),
            $container->get({RelationService}AppControllerConfig::class),
        );
    },

    '{relation_service}_create_app_action' => static function (ContainerInterface $container): CreateAppAction
    {
        return new CreateAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_create_multiple_app_action' => static function (ContainerInterface $container): CreateMultipleAppAction
    {
        return new CreateMultipleAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_delete_app_action' => static function (ContainerInterface $container): DeleteAppAction
    {
        return new DeleteAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_delete_multiple_app_action' => static function (ContainerInterface $container): DeleteMultipleAppAction
    {
        return new DeleteMultipleAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_get_all_app_action' => static function (ContainerInterface $container): GetAllAppAction
    {
        return new GetAllAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_get_all_paginated_app_action' => static function (ContainerInterface $container): GetAllPaginatedAppAction
    {
        return new GetAllPaginatedAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_get_one_by_id_app_action' => static function (ContainerInterface $container): GetOneByIdAppAction
    {
        return new GetOneByIdAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_update_app_action' => static function (ContainerInterface $container): UpdateAppAction
    {
        return new UpdateAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },

    '{relation_service}_update_multiple_app_action' => static function (ContainerInterface $container): UpdateMultipleAppAction
    {
        return new UpdateMultipleAppAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get({RelationService}AppActionConfig::class),
        );
    },
];
