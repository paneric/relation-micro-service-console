<?php

declare(strict_types=1);

use {Paneric}\{RelationService}\config\{RelationService}RepositoryConfig;
use {Paneric}\{RelationService}\Repository\{RelationService}Repository;
use {Paneric}\{RelationService}\Repository\{RelationService}RepositoryInterface;
use Paneric\DBAL\Manager;
use Psr\Container\ContainerInterface;

return [
    {RelationService}RepositoryInterface::class => static function (ContainerInterface $container): {RelationService}Repository {
        return new {RelationService}Repository(
            $container->get(Manager::class),
            $container->get({RelationService}RepositoryConfig::class)
        );
    },
];
