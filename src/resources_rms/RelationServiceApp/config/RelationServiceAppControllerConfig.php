<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\{RelationService}App\config;

use Paneric\Interfaces\Config\ConfigInterface;

class {RelationService}AppControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => '{prefix}'
        ];
    }
}
