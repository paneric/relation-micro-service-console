<?php

declare(strict_types=1);

use {Paneric}\{RelationService}\Gateway\{RelationService}ADTO;

return [

    'validation' => [

        '{prefix}.add' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        '{prefix}s.add' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        '{prefix}.edit' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        '{prefix}s.edit' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        '{prefix}.remove' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        '{prefix}s.remove' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
