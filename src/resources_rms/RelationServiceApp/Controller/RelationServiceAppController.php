<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\{RelationService}App\Controller;

use Paneric\ComponentModule\Module\Controller\Relation\RelationModuleAppController;

class {RelationService}AppController extends RelationModuleAppController {}
