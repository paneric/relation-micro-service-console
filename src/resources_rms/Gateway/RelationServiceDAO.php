<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\Gateway;

{daoUseRelations}
use Paneric\DataObject\ADAO;

class {RelationService}DAO extends ADAO
{
    protected $id;
{daoRelationsId}
{daoRelations}
{daoAttributes}
    public function __construct()
    {
        $this->prefix = '{prefix}_';

        $this->setMaps();

        if ($this->values) {
{daoRelationsSeed}
            $this->hydrate($this->values);

            unset($this->values);
        }
    }

    /**
     * @return null|int|string
     */
    public function getId()
    {
        return $this->id;
    }
{daoGettersRelationsId}
{daoGettersRelations}
{daoGettersAttributes}
    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
{daoSettersRelationsId}
{daoSettersRelations}
{daoSettersAttributes}
}
