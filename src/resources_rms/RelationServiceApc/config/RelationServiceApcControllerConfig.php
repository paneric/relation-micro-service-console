<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\{RelationService}Apc\config;

use Paneric\Interfaces\Config\ConfigInterface;

class {RelationService}ApcControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => '{prefix}'
        ];
    }
}
