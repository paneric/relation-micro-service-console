<?php

declare(strict_types=1);

use Paneric\ComponentModule\Module\Action\Apc\GetAllApcAction;
use Paneric\ComponentModule\Module\Action\SubData;

use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;

{subDataUseSubServices}
return [
    '{relation_service}_apc_sub_data' => static function (ContainerInterface $container): SubData
    {
{subDataSubServiceCreate}
        return new SubData([
{subDataSubService}
        ]);
    },
];
