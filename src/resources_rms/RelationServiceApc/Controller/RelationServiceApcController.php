<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\{RelationService}Apc\Controller;

use Paneric\ComponentModule\Module\Controller\Relation\RelationModuleApcController;

class {RelationService}ApcController extends RelationModuleApcController {}
