<?php

declare(strict_types=1);

use {Paneric}\{RelationService}\{RelationService}Apc\Controller\{RelationService}ApcController;
use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;

if (isset($app, $container)) {

    $app->map(['GET'], '/apc-{prefix}/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get({RelationService}ApcController::class)->showOneById(
            $request,
            $response,
            $this->get('{relation_service}_get_one_by_id_apc_action'),
            $args['id'] ?? '1'
        );
    })->setName('apc-{prefix}.show-one-by-id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET'],'/apc-{prefix}s/show-all', function(Request $request, Response $response) {
        return $this->get({RelationService}ApcController::class)->showAll(
            $request,
            $response,
            $this->get('{relation_service}_get_all_apc_action')
        );
    })->setName('apc-{prefix}s.show-all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET'],'/apc-{prefix}s/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get({RelationService}ApcController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('{relation_service}_get_all_paginated_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-{prefix}s.show-all-paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class)); // no pagination middleware !!!


    $app->map(['GET', 'POST'], '/apc-{prefix}/add', function(Request $request, Response $response) {
        return $this->get({RelationService}ApcController::class)->add(
            $request,
            $response,
            $this->get('{relation_service}_apc_sub_data'),
            $this->get('{relation_service}_create_apc_action')
        );
    })->setName('apc-{prefix}.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-{prefix}s/add', function(Request $request, Response $response) {
        return $this->get({RelationService}ApcController::class)->addMultiple(
            $request,
            $response,
            $this->get('{relation_service}_apc_sub_data'),
            $this->get('{relation_service}_create_multiple_apc_action')
        );
    })->setName('apc-{prefix}s.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-{prefix}/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get({RelationService}ApcController::class)->edit(
            $request,
            $response,
            $this->get('{relation_service}_apc_sub_data'),
            $this->get('{relation_service}_get_one_by_id_apc_action'),
            $this->get('{relation_service}_update_apc_action'),
            $args['id']
        );
    })->setName('apc-{prefix}.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-{prefix}s/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get({RelationService}ApcController::class)->editMultiple(
            $request,
            $response,
            $this->get('{relation_service}_apc_sub_data'),
            $this->get('{relation_service}_get_all_paginated_apc_action'),
            $this->get('{relation_service}_update_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-{prefix}s.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-{prefix}/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get({RelationService}ApcController::class)->remove(
            $request,
            $response,
            $this->get('{relation_service}_delete_apc_action'),
            $args['id']
        );
    })->setName('apc-{prefix}.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-{prefix}s/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get({RelationService}ApcController::class)->removeMultiple(
            $request,
            $response,
            $this->get('{relation_service}_get_all_paginated_apc_action'),
            $this->get('{relation_service}_delete_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-{prefix}s.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}

