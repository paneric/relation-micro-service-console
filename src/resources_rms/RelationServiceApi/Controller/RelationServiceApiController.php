<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\{RelationService}Api\Controller;

use Paneric\ComponentModule\Module\Controller\Relation\RelationModuleApiController;

class {RelationService}ApiController extends RelationModuleApiController {}
