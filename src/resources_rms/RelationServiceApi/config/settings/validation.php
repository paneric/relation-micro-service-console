<?php

declare(strict_types=1);

use {Paneric}\{RelationService}\Gateway\{RelationService}ADTO;

return [

    'validation' => [

        'api-{prefix}.create' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        'api-{prefix}s.create' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        'api-{prefix}.update' => [
            'methods' => ['PUT'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        'api-{prefix}s.update' => [
            'methods' => ['PUT'],
            {RelationService}ADTO::class => [
                'rules' => [
{validationRelationsId}
{validationAttributes}
                ],
            ],
        ],

        'api-{prefix}s.delete' => [
            'methods' => ['POST'],
            {RelationService}ADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ]
                ],
            ],
        ],
    ]
];
