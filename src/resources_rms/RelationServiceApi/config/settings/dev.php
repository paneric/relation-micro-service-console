<?php

declare(strict_types=1);

use {Paneric}\{RelationService}\Repository\{RelationService}RepositoryInterface;
use Paneric\ComponentModule\Interfaces\Repository\BaseServiceApiRepositoryInterface;

return [
    'root_folder' => ROOT_FOLDER,

    'base_uri' => '',

    'main_route_name' => 'cms.main.index',

    'dbal' => [
        'limit' => 10,
        'host' => $_ENV['DB_HOST'],
        'charset' => 'utf8',
        'dbName' => $_ENV['DB_NAME'],
        'user' => $_ENV['DB_USR'],
        'password' => $_ENV['DB_PSWD'],
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::MYSQL_ATTR_FOUND_ROWS => true//count rowCounts even if identical values updated
        ],
    ],

    'session-wrapper' => [
        'table' => 'sessions',

        'cookie_lifetime' => '0',         // Expire on close (string value!!!)
        'cookie_access' => '/',         // SessionWrapper cookie readable in all folders.
        'domain' => '',                 // '' for localhost, '.exemple.com' for others
        'secure' => false,              // in case of https true
        'js_denied' => true,            // Make sure the session cookie is not accessible via javascript.

        'hash_function' => 'sha512',    // Hash algorithm to use for the session. (use hash_algos() to get a list of available hashes.)
        'hash_bits_per_character' => 6, // How many bits per character of the hash. The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
        'use_only_cookies' => 1,        // Force the session to only use cookies, not URL variables.
        'gc_maxlifetime' => '86400',      // session max lifetime

        'cookie_name' => 'scm',         // session cookie name

        'encrypt' => true,
        'regenerate' => false,          // Adviced true, but somehow ajax searcher does not work with it.
    ],

    'guard' => [
        'key_ascii' => $_ENV['KEY_ASCII'],
        'algo_password' => PASSWORD_BCRYPT,
        'options_algo_password' => [
            'cost' => 10,
        ],
        'algo_hash' => 'sha512',
        'unique_id_prefix' => '',
        'unique_id_more_entropy' => true,
    ],

    'pagination-middleware' => [
        'api-{prefix}s.get.page' => {RelationService}RepositoryInterface::class,//middleware
        'page_rows_number' => 20,//middleware
    ],

    'csrf' => [
        'csrf_key_name' => 'csrf_key',
        'csrf_key_length' => 32,
        'csrf_hash_name' => 'csrf_hash',
    ],

    'jwt_authentication' => [
        'secret' => $_ENV['JWT_SECRET'],
        'algorithm' => 'HS256',
        'secure' => false, // only for localhost for prod and test env set true
        'error' => static function ($response, $arguments) {
            $data['status'] = 401;
            $data['error'] = 'Unauthorized/'. $arguments['message'];
            return $response
                ->withHeader('Content-Type', 'application/json;charset=utf-8')
                ->getBody()->write(json_encode(
                    $data,
                    JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                ));
        }
    ],
];
