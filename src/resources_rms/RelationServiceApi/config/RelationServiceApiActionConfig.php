<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\{RelationService}Api\config;

use {Paneric}\{RelationService}\Gateway\{RelationService}DAO;
use {Paneric}\{RelationService}\Gateway\{RelationService}DTO;
use Paneric\Interfaces\Config\ConfigInterface;

class {RelationService}ApiActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'get_one_by_id' => [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['{prefix}_id' => (int) $id];
                },
            ],

            'get_all' => [
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'get_all_paginated' => [
                'find_by_criteria' => static function (string $local = null): array
                {
                    return [];
                },
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'create' => [
                'dao_class' => {RelationService}DAO::class,
                'dto_class' => {RelationService}DTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return [
{uniqueCriteriaFromAttributes}
                    ];
                },
            ],

            'create_multiple' => [
                'dao_class' => {RelationService}DAO::class,
                'dto_class' => {RelationService}DTO::class,
                'create_unique_criteria' => static function (array $collection): array
                {
                    $createUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $createUniqueCriteria[$index] = [
{uniqueCriteriaFromDAO}
                        ];
                    }

                    return $createUniqueCriteria;
                },
            ],

            'update' => [
                'dao_class' => {RelationService}DAO::class,
                'dto_class' => {RelationService}DTO::class,
                'find_one_by_criteria' => static function ({RelationService}DAO $dao, string $id): array
                {
                    return [
                        '{prefix}_id' => (int) $id,
{uniqueCriteriaFromDAO}
                    ];
                },
                'update_unique_criteria' => static function (string $id): array
                {
                    return ['{prefix}_id' => (int) $id];
                },
            ],

            'update_multiple' => [
                'dao_class' => {RelationService}DAO::class,
                'dto_class' => {RelationService}DTO::class,
                'find_by_criteria' => static function (array $collection): array
                {
                    $findByCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $findByCriteria[$index] = [
                            '{prefix}_id' => (int) $index,
{uniqueCriteriaFromDAO}
                        ];
                    }

                    return $findByCriteria;
                },
                'update_unique_criteria' => static function (array $collection): array
                {
                    $updateUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $updateUniqueCriteria[$index] = [
                            '{prefix}_id' => (int) $index,
                        ];
                    }

                    return $updateUniqueCriteria;
                },
            ],

            'delete' => [
                'delete_by_criteria' => static function (array $attributes): array
                {
                    $deleteByCriteria = [];

                    foreach ($attributes as $key => $value) {
                        $deleteByCriteria['{prefix}_' . $key] = (int) $value;
                    }

                    return $deleteByCriteria;
                },
            ],

            'delete_multiple' => [
                'dao_class' => {RelationService}DAO::class,
                'dto_class' => {RelationService}DTO::class,
                'delete_by_criteria' => static function (array $collection): array
                {
                    $deleteByCriteria = [];

                    foreach ($collection as $index => $dao) {
                            $deleteByCriteria[$index]['{prefix}_id'] = (int) $dao->getId();
                    }

                    return $deleteByCriteria;
                },
            ],
        ];
    }
}
