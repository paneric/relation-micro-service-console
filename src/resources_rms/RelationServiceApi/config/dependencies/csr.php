<?php

use {Paneric}\{RelationService}\{RelationService}Api\config\{RelationService}ApiActionConfig;
use {Paneric}\{RelationService}\Repository\{RelationService}RepositoryInterface;
use Paneric\ComponentModule\Module\Action\Api\CreateApiAction;
use Paneric\ComponentModule\Module\Action\Api\CreateMultipleApiAction;
use Paneric\ComponentModule\Module\Action\Api\DeleteApiAction;
use Paneric\ComponentModule\Module\Action\Api\DeleteMultipleApiAction;
use Paneric\ComponentModule\Module\Action\Api\GetAllApiAction;
use Paneric\ComponentModule\Module\Action\Api\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Module\Action\Api\GetOneByIdApiAction;
use Paneric\ComponentModule\Module\Action\Api\UpdateApiAction;
use Paneric\ComponentModule\Module\Action\Api\UpdateMultipleApiAction;
use Psr\Container\ContainerInterface;

return [

    '{relation_service}_create_api_action' => static function (ContainerInterface $container): CreateApiAction
    {
        return new CreateApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_create_multiple_api_action' => static function (ContainerInterface $container): CreateMultipleApiAction
    {
        return new CreateMultipleApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_delete_api_action' => static function (ContainerInterface $container): DeleteApiAction
    {
        return new DeleteApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_delete_multiple_api_action' => static function (ContainerInterface $container): DeleteMultipleApiAction
    {
        return new DeleteMultipleApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_get_all_api_action' => static function (ContainerInterface $container): GetAllApiAction
    {
        return new GetAllApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_get_all_paginated_api_action' => static function (ContainerInterface $container): GetAllPaginatedApiAction
    {
        return new GetAllPaginatedApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_get_one_by_id_api_action' => static function (ContainerInterface $container): GetOneByIdApiAction
    {
        return new GetOneByIdApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_update_api_action' => static function (ContainerInterface $container): UpdateApiAction
    {
        return new UpdateApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },

    '{relation_service}_update_multiple_api_action' => static function (ContainerInterface $container): UpdateMultipleApiAction
    {
        return new UpdateMultipleApiAction (
            $container->get({RelationService}RepositoryInterface::class),
            $container->get({RelationService}ApiActionConfig::class),
        );
    },
];
