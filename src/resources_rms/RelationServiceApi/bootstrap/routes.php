<?php

declare(strict_types=1);

use {Paneric}\{RelationService}\{RelationService}Api\Controller\{RelationService}ApiController;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->map(['GET'], '/api-{prefix}/get/{id}', function(Request $request, Response $response, array $args){
        return $this->get({RelationService}ApiController::class)->getOneById(
            $request,
            $response,
            $this->get('{relation_service}_get_one_by_id_api_action'),
            $args['id']
        );
    })->setName('api-{prefix}.get')
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->map(['GET'], '/api-{prefix}s/get', function(Request $request, Response $response){
        return $this->get({RelationService}ApiController::class)->getAll(
            $request,
            $response,
            $this->get('{relation_service}_get_all_api_action')
        );
    })->setName('api-{prefix}s.get')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->map(['GET'], '/api-{prefix}s/get/{page}', function(Request $request, Response $response, array $args){
        return $this->get({RelationService}ApiController::class)->getAllPaginated(
            $request,
            $response,
            $this->get('{relation_service}_get_all_paginated_api_action'),
            $args['page']
        );
    })->setName('api-{prefix}s.get.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->post('/api-{prefix}/create', function(Request $request, Response $response){
        return $this->get({RelationService}ApiController::class)->create(
            $request,
            $response,
            $this->get('{relation_service}_create_api_action')
        );
    })->setName('api-{prefix}.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-{prefix}s/create', function(Request $request, Response $response){
        return $this->get({RelationService}ApiController::class)->createMultiple(
            $request,
            $response,
            $this->get('{relation_service}_create_multiple_api_action')
        );
    })->setName('api-{prefix}s.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->put('/api-{prefix}/update/{id}', function(Request $request, Response $response, array $args){
        return $this->get({RelationService}ApiController::class)->update(
            $request,
            $response,
            $this->get('{relation_service}_update_api_action'),
            $args['id']
        );
    })->setName('api-{prefix}.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-{prefix}s/update', function(Request $request, Response $response){
        return $this->get({RelationService}ApiController::class)->updateMultiple(
            $request,
            $response,
            $this->get('{relation_service}_update_multiple_api_action')
        );
    })->setName('api-{prefix}s.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->delete('/api-{prefix}/delete/{id}', function(Request $request, Response $response, array $args){
        return $this->get({RelationService}ApiController::class)->delete(
            $request,
            $response,
            $this->get('{relation_service}_delete_api_action'),
            $args['id']
        );
    })->setName('api-{prefix}.delete')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-{prefix}s/delete', function(Request $request, Response $response){
        return $this->get({RelationService}ApiController::class)->deleteMultiple(
            $request,
            $response,
            $this->get('{relation_service}_delete_multiple_api_action')
        );
    })->setName('api-{prefix}s.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));
}
