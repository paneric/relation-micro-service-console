<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\config;

use {Paneric}\{RelationService}\Gateway\{RelationService}DAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class RelationServiceRepositoryConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'table' => '{relation_service}s',
            'dao_class' => {RelationService}DAO::class,
            'fetch_mode' => PDO::FETCH_CLASS,//Data populated before dao constructor
            'create_unique_where' => sprintf(
                ' %s',
{uniqueCriteriaSQLWhere}
            ),
            'update_unique_where' => sprintf(
                ' %s %s',
{uniqueCriteriaSQLWhere},
                'AND {prefix}_id NOT IN (:{prefix}_id)'
            ),
            'select_query' => '
                SELECT *
                FROM {relation_service}s bt
{innerJoinSubService}
            ',
        ];
    }
}
