<?php

declare(strict_types=1);

use Paneric\ModuleResolver\ModuleMapper;

$moduleMapper = new ModuleMapper();

$moduleMapCrossPaths = [
{vendorSubServicesPaths}
];

$moduleMapPath = '{vendorRelationServicePath}';

$moduleMap = [
    '{prefix}'      => ROOT_FOLDER . '%s/src/{RelationService}App',
    '{prefix}s'     => ROOT_FOLDER . '%s/src/{RelationService}App',
    'api-{prefix}'  => ROOT_FOLDER . '%s/src/{RelationService}Api',
    'api-{prefix}s' => ROOT_FOLDER . '%s/src/{RelationService}Api',
    'apc-{prefix}'  => ROOT_FOLDER . '%s/src/{RelationService}Apc',
    'apc-{prefix}s' => ROOT_FOLDER . '%s/src/{RelationService}Apc',
];

return [
    'default_route_key' => '{prefix}',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => true,

    'module_map' => $moduleMapper->setModuleMap($moduleMap),

    'module_map_cross' => $moduleMapper->setModuleMapCross(
        $moduleMapCrossPaths,
        $moduleMap,
        $moduleMapPath,
        __DIR__ !== ROOT_FOLDER . 'src/config'
    ),
];
