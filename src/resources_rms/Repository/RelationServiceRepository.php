<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\Repository;

use Paneric\ComponentModule\Module\Repository\ModuleRepository;

class {RelationService}Repository extends ModuleRepository implements {RelationService}RepositoryInterface {}
