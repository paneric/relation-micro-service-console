<?php

declare(strict_types=1);

namespace {Paneric}\{RelationService}\Repository;

use Paneric\ComponentModule\Interfaces\Repository\ModuleRepositoryInterface;

interface {RelationService}RepositoryInterface extends ModuleRepositoryInterface
{}
